# FilePicker Library for Android

A FilePicker library for Android for selecting multiple types of files and also to capture Images and Videos.

[Sample Apk](https://gitlab.com/joielechong/FilePicker/)

## Note

* Uses [MediaStore](https://developer.android.com/reference/android/provider/MediaStore) Database for loading files info.
* Uses [Glide](https://github.com/bumptech/glide) for loading images.

## [Screenshots](pics/)

![pic](pics/pic.gif)

## Usage

Step 1: Add it in your root build.gradle at the end of repositories

```gradle
    allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
```

Step 2: Add the dependency

```gradle
    dependencies {
        ...
        implementation 'com.gitlab.joielechong:FilePicker:1.1.2'
    }
```

Step 3: Call the FilePicker with the following code:

```java
        FilePicker.with(MainActivity.this)
            .enableImageCapture(true) // show selection to use camera
            .showVideos(false) // dont' show video files
            .skipZeroSizeFiles(true) // don't show zero size files
            .maxSelection(10) // maximum 10 selection of files.
            .resultListener(new FilePicker.ResultListener() {
              @Override public void onMediaFileSelected(List<MediaFile> mediaFiles) {
                // Selected files is mediaFiles
                // handle it here.
              }
            })
            .show(); // show the picker.
```

Step 4: Handle the permission manually (Please take a look at the sample app project)

### Configuration Methods

|Methods|Default value|Uses|
|-------|-------|---|
|title(String)|""|Use string as the title of FilePicker|
|showFileCount(boolean)|true|Whether to show file count and selected of FilePicker|
|showImages(boolean)|true|Whether to load Images files|
|showVideos(boolean)|true|Whether to load Videos files|
|showAudios(boolean)|false|Whether to load Audio files|
|showFiles(boolean)|false|Whether to load Files for given suffixes|
|enableImageCapture(boolean)|false|Enables camera for capturing of images|
|enableVideoCapture(boolean)|false|Enables camera for capturing of videos|
|suffixes(String...)|"txt", "pdf", "html", "rtf", "csv", "xml",<br/>"zip", "tar", "gz", "rar", "7z","torrent",<br/>"doc", "docx", "odt", "ott",<br/>"ppt", "pptx", "pps",<br/>"xls", "xlsx", "ods", "ots"|Suffixes for file to be loaded, overrides default value|
|maxSelection(int)|-1|Maximum no of items to be selected, -1 for no limits|
|selectedMediaFiles(ArrayList\<MediaFile\>)|null|Default files to be marked as selected|
|singleClickSelection(boolean)|true|Start selection mode on single click else on long click|
|skipZeroSizeFiles(boolean)|true|Whether to load zero byte sized files|
|landscapeSpanCount(int)|5|Grid items in landscape mode|
|portraitSpanCount(int)|3|Grid items in portrait mode|
|imageSize(int)|Screen width/portraitSpanCount |Size of height, width of image to be loaded in Px|

## MediaFile methods

|Method|Description|
|------|-----------|
|long getId()|Id of the file in MediaStore database|
|String getName()|Name of file without suffix|
|String getPath()|Absolute path of the file|
|long getSize()|Size of the file|
|long getDate()|Date when file is added to MediaStore database|
|String getMimeType()|Mime type of the file|
|int getMediaType()|One of TYPE_FILE, TYPE_IMAGE, TYPE_AUDIO, TYPE_VIDEO|
|long getDuration()|Duration of Audio, Video files in ms, else 0|
|Uri getThumbnail()|Album Art of Audio files|
|long getHeight()|Height of Image, Video files in Px for API>=16, else 0|
|long getWidth()|Width of Image, Video files in Px for API>=16, else 0|
|String getBucketId()|Id of Parent Directory in MediaStore database|
|String getBucketName()|Name of Parent Directory|

## Contributions

Feel free to contribute to this project. Before creating issues or pull request please take a look at following templates.

* [Bug report template](.github/ISSUE_TEMPLATE/bug_report.md)
* [Feature request template](.github/ISSUE_TEMPLATE/feature_request.md)
* [Pull Request template](.github/PULL_REQUEST_TEMPLATE.md)

## Credits

* Inspired by [MultiType-FilePicker](https://github.com/fishwjy/MultiType-FilePicker)

## License

    Copyright (c) 2018, Rilix Technology

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
