package com.rilixtech.filepicker.loader;

import android.support.annotation.Nullable;

import com.rilixtech.filepicker.MediaFile;

import java.util.ArrayList;

public interface FileResultCallback {
  void onResult(@Nullable ArrayList<MediaFile> mediaFiles);
}
