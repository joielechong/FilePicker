package com.rilixtech.filepicker.loader;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.rilixtech.filepicker.Config;
import com.rilixtech.filepicker.MediaFile;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static android.provider.MediaStore.Audio.AlbumColumns.ALBUM_ID;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE;
import static android.provider.MediaStore.Files.FileColumns.MIME_TYPE;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME;
import static android.provider.MediaStore.Images.ImageColumns.BUCKET_ID;
import static android.provider.MediaStore.MediaColumns.DATA;
import static android.provider.MediaStore.MediaColumns.DATE_ADDED;
import static android.provider.MediaStore.MediaColumns.HEIGHT;
import static android.provider.MediaStore.MediaColumns.SIZE;
import static android.provider.MediaStore.MediaColumns.TITLE;
import static android.provider.MediaStore.MediaColumns.WIDTH;
import static android.provider.MediaStore.Video.VideoColumns.DURATION;

class FileLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {
  private Context context;
  private FileResultCallback fileResultCallback;
  private Config configs;

  FileLoaderCallback(@NonNull Context context, @NonNull FileResultCallback fileResultCallback,
      @NonNull Config configs) {
    this.context = context;
    this.fileResultCallback = fileResultCallback;
    this.configs = configs;
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    return new FileLoader(context, configs);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    ArrayList<MediaFile> mediaFiles = new ArrayList<>();
    if (data.moveToFirst()) {
      do {
        long size = data.getLong(data.getColumnIndex(SIZE));
        if (size == 0) {
          //Check if File size is really zero
          size = new java.io.File(data.getString(data.getColumnIndex(DATA))).length();
          if (size <= 0 && configs.isSkipZeroSizeFiles()) {
            continue;
          }
        }
        MediaFile mediaFile = new MediaFile();
        mediaFile.setSize(size);
        mediaFile.setId(data.getLong(data.getColumnIndex(_ID)));
        mediaFile.setName(data.getString(data.getColumnIndex(TITLE)));
        mediaFile.setPath(data.getString(data.getColumnIndex(DATA)));
        mediaFile.setDate(data.getLong(data.getColumnIndex(DATE_ADDED)));
        mediaFile.setMimeType(data.getString(data.getColumnIndex(MIME_TYPE)));
        mediaFile.setMediaType(data.getInt(data.getColumnIndex(MEDIA_TYPE)));
        mediaFile.setBucketId(data.getString(data.getColumnIndex(BUCKET_ID)));
        mediaFile.setBucketName(data.getString(data.getColumnIndex(BUCKET_DISPLAY_NAME)));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
          mediaFile.setHeight(data.getLong(data.getColumnIndex(HEIGHT)));
          mediaFile.setWidth(data.getLong(data.getColumnIndex(WIDTH)));
        }
        mediaFile.setDuration(data.getLong(data.getColumnIndex(DURATION)));
        int albumID = data.getInt(data.getColumnIndex(ALBUM_ID));
        if (albumID > 0) {
          mediaFile.setThumbnail(ContentUris
              .withAppendedId(Uri.parse("content://media/external/audio/albumart"),
                  albumID));
        }
        mediaFiles.add(mediaFile);
      } while (data.moveToNext());
    }
    fileResultCallback.onResult(mediaFiles);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
  }
}
