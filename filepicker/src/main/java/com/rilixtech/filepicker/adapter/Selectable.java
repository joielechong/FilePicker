package com.rilixtech.filepicker.adapter;

public interface Selectable {
  boolean isSelected();
  void setSelected(boolean selected);
}
