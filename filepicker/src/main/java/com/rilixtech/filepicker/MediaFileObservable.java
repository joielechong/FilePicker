package com.rilixtech.filepicker;

import java.util.Observable;

class MediaFileObservable extends Observable {

  private static MediaFileObservable instance;

  private MediaFileObservable() {
  }

  public static MediaFileObservable getInstance() {
    if (instance == null) {
      synchronized (MediaFileObservable.class) {
        if (instance == null) {
          instance = new MediaFileObservable();
        }
      }
    }
    return instance;
  }

  void updateValue(Object data) {
    synchronized (this) {
      setChanged();
      notifyObservers(data);
    }
  }
}
