package com.rilixtech.filepicker;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public class Config implements Parcelable {
  public static final Creator<Config> CREATOR = new Creator<Config>() {
    @Override
    public Config createFromParcel(Parcel in) {
      return new Config(in);
    }

    @Override
    public Config[] newArray(int size) {
      return new Config[size];
    }
  };
  private final boolean imageCaptureEnabled;
  private final boolean videoCaptureEnabled;
  private final boolean showVideos;
  private final boolean showImages;
  private final boolean showAudios;
  private final boolean showFiles;
  private final boolean singleClickSelection;
  private final boolean skipZeroSizeFiles;
  private final int imageSize, maxSelection;
  private final int landscapeSpanCount;
  private final int portraitSpanCount;
  private final String[] suffixes;
  private final List<MediaFile> selectedMediaFiles;
  private final String title;
  private final boolean showFileCount;

  public Config(boolean imageCapture, boolean videoCapture,
      boolean showVideos, boolean showImages, boolean showAudios, boolean showFiles,
      boolean singleClickSelection, boolean skipZeroSizeFiles,
      int imageSize, int maxSelection, int landscapeSpanCount, int portraitSpanCount,
      String[] suffixes, List<MediaFile> selectedMediaFiles, String title, boolean showFileCount) {
    this.imageCaptureEnabled = imageCapture;
    this.videoCaptureEnabled = videoCapture;
    this.showVideos = showVideos;
    this.showImages = showImages;
    this.showAudios = showAudios;
    this.showFiles = showFiles;
    this.singleClickSelection = singleClickSelection;
    this.skipZeroSizeFiles = skipZeroSizeFiles;
    this.imageSize = imageSize;
    this.maxSelection = maxSelection;
    this.landscapeSpanCount = landscapeSpanCount;
    this.portraitSpanCount = portraitSpanCount;
    this.suffixes = suffixes;
    this.selectedMediaFiles = selectedMediaFiles;
    this.title = title;
    this.showFileCount = showFileCount;
  }

  protected Config(Parcel in) {
    imageCaptureEnabled = in.readByte() != 0;
    videoCaptureEnabled = in.readByte() != 0;
    showVideos = in.readByte() != 0;
    showImages = in.readByte() != 0;
    showAudios = in.readByte() != 0;
    showFiles = in.readByte() != 0;
    singleClickSelection = in.readByte() != 0;
    skipZeroSizeFiles = in.readByte() != 0;
    imageSize = in.readInt();
    maxSelection = in.readInt();
    landscapeSpanCount = in.readInt();
    portraitSpanCount = in.readInt();
    suffixes = in.createStringArray();
    selectedMediaFiles = in.createTypedArrayList(MediaFile.CREATOR);
    title = in.readString();
    showFileCount = in.readByte() != 0;
  }

  public boolean isShowVideos() {
    return showVideos;
  }

  public boolean isShowImages() {
    return showImages;
  }

  public boolean isShowAudios() {
    return showAudios;
  }

  public boolean isShowFiles() {
    return showFiles;
  }

  public boolean isSkipZeroSizeFiles() {
    return skipZeroSizeFiles;
  }

  public List<MediaFile> getSelectedMediaFiles() {
    return selectedMediaFiles;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeByte((byte) (imageCaptureEnabled ? 1 : 0));
    dest.writeByte((byte) (videoCaptureEnabled ? 1 : 0));
    dest.writeByte((byte) (showVideos ? 1 : 0));
    dest.writeByte((byte) (showImages ? 1 : 0));
    dest.writeByte((byte) (showAudios ? 1 : 0));
    dest.writeByte((byte) (showFiles ? 1 : 0));
    dest.writeByte((byte) (singleClickSelection ? 1 : 0));
    dest.writeByte((byte) (skipZeroSizeFiles ? 1 : 0));
    dest.writeInt(imageSize);
    dest.writeInt(maxSelection);
    dest.writeInt(landscapeSpanCount);
    dest.writeInt(portraitSpanCount);
    dest.writeStringArray(suffixes);
    dest.writeTypedList(selectedMediaFiles);
    dest.writeString(title);
    dest.writeByte((byte) (showFileCount ? 1 : 0));
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public boolean isSingleClickSelection() {
    return singleClickSelection;
  }

  public int getMaxSelection() {
    return maxSelection;
  }

  public int getLandscapeSpanCount() {
    return landscapeSpanCount;
  }

  public int getPortraitSpanCount() {
    return portraitSpanCount;
  }

  public boolean isImageCaptureEnabled() {
    return imageCaptureEnabled;
  }

  public boolean isVideoCaptureEnabled() {
    return videoCaptureEnabled;
  }

  public int getImageSize() {
    return imageSize;
  }

  public String[] getSuffixes() {
    return suffixes;
  }

  public String getTitle() {
    return title;
  }

  public boolean isShowFileCount() {
    return showFileCount;
  }
}
