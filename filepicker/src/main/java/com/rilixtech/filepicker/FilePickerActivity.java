package com.rilixtech.filepicker;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.rilixtech.filepicker.adapter.FileGalleryAdapter;
import com.rilixtech.filepicker.adapter.MultiSelectionAdapter;
import com.rilixtech.filepicker.loader.FileLoader;
import com.rilixtech.filepicker.loader.FileResultCallback;
import com.rilixtech.filepicker.view.DividerItemDecoration;
import java.io.File;
import java.util.ArrayList;

public class FilePickerActivity extends AppCompatActivity
    implements MultiSelectionAdapter.OnSelectionListener<FileGalleryAdapter.ViewHolder> {
  public static final String MEDIA_FILES = "MEDIA_FILES";
  public static final String CONFIGS = "CONFIGS";
  private static final String TAG = "FilePickerActivity";
  private static final String PATH = "PATH";
  private static final String URI = "URI";

  private Config configs;
  private ArrayList<MediaFile> mediaFiles = new ArrayList<>();
  private FileGalleryAdapter fileGalleryAdapter;
  private int maxCount;
  private boolean mShowFileCount;
  private String mTitle;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.filepicker_gallery);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    configs = getIntent().getParcelableExtra(CONFIGS);

    int spanCount;
    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
      spanCount = configs.getLandscapeSpanCount();
    } else {
      spanCount = configs.getPortraitSpanCount();
    }

    mTitle = configs.getTitle();
    mShowFileCount = configs.isShowFileCount();

    int imageSize = configs.getImageSize();
    if (imageSize <= 0) {
      Point point = new Point();
      getWindowManager().getDefaultDisplay().getSize(point);
      imageSize = Math.min(point.x, point.y) / configs.getPortraitSpanCount();
    }

    fileGalleryAdapter = new FileGalleryAdapter(this, mediaFiles, imageSize,
        configs.isImageCaptureEnabled(),
        configs.isVideoCaptureEnabled());
    fileGalleryAdapter.enableSelection(true);
    fileGalleryAdapter.enableSingleClickSelection(configs.isSingleClickSelection());
    fileGalleryAdapter.setOnSelectionListener(this);
    fileGalleryAdapter.setMaxSelection(configs.getMaxSelection());
    fileGalleryAdapter.setSelectedItems(configs.getSelectedMediaFiles());
    RecyclerView recyclerView = findViewById(R.id.file_gallery);
    recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
    recyclerView.setAdapter(fileGalleryAdapter);
    recyclerView.addItemDecoration(new DividerItemDecoration(this));

    if (savedInstanceState == null) {
      loadFiles(false);
    } else {
      ArrayList<MediaFile> mediaFiles = savedInstanceState.getParcelableArrayList(MEDIA_FILES);
      if (mediaFiles != null) {
        this.mediaFiles.clear();
        this.mediaFiles.addAll(mediaFiles);
        fileGalleryAdapter.getSelectedItems().clear();
        fileGalleryAdapter.notifyDataSetChanged();
      }
    }

    maxCount = configs.getMaxSelection();

    setActivityTitle();
  }

  private void loadFiles(boolean restart) {
    FileLoader.loadFiles(this, new FileResultCallback() {
      @Override
      public void onResult(ArrayList<MediaFile> filesResults) {
        if (filesResults != null) {
          mediaFiles.clear();
          mediaFiles.addAll(filesResults);
          fileGalleryAdapter.notifyDataSetChanged();
        }
      }
    }, configs, restart);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == FileGalleryAdapter.CAPTURE_IMAGE_VIDEO) {
      File file = fileGalleryAdapter.getLastCapturedFile();
      if (resultCode == RESULT_OK) {
        MediaScannerConnection.scanFile(this, new String[] { file.getAbsolutePath() }, null,
            new MediaScannerConnection.OnScanCompletedListener() {
              @Override
              public void onScanCompleted(String path, final Uri uri) {
                if (uri != null) {
                  runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      loadFiles(true);
                    }
                  });
                }
              }
            });
      } else {
        getContentResolver().delete(fileGalleryAdapter.getLastCapturedUri(),
            null, null);
      }
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.filegallery_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.done) {
      MediaFileObservable.getInstance().updateValue(fileGalleryAdapter.getSelectedItems());
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putParcelableArrayList(MEDIA_FILES, mediaFiles);
    File file = fileGalleryAdapter.getLastCapturedFile();
    if (file != null) {
      outState.putString(PATH, file.getAbsolutePath());
    }
    outState.putParcelable(URI, fileGalleryAdapter.getLastCapturedUri());
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    String path = savedInstanceState.getString(PATH);
    if (path != null) fileGalleryAdapter.setLastCapturedFile(new File(path));

    Uri uri = savedInstanceState.getParcelable(URI);
    if (uri != null) fileGalleryAdapter.setLastCapturedUri(uri);
  }

  @Override
  public void onSelectionBegin() {

  }

  @Override
  public void onSelected(FileGalleryAdapter.ViewHolder viewHolder, int position) {
    setActivityTitle();
  }

  @Override
  public void onUnSelected(FileGalleryAdapter.ViewHolder viewHolder, int position) {
    setActivityTitle();
  }

  private void setActivityTitle() {
    String title = "";
    if(mShowFileCount && maxCount > 0) {
      title = getString(R.string.selection_count, fileGalleryAdapter.getSelectedItemCount(), maxCount);
    }

    title = mTitle.isEmpty() ? "" : getString(R.string.selection_count_with_title, title, mTitle);
    setTitle(title);
  }

  @Override
  public void onSelectAll() {

  }

  @Override
  public void onUnSelectAll() {

  }

  @Override
  public void onSelectionEnd() {

  }

  @Override
  public void onMaxReached() {
  }
}
