package com.rilixtech.filepicker;

import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class FilePicker implements Observer {
  private boolean imageCaptureEnabled;
  private boolean videoCaptureEnabled;
  private boolean showVideos;
  private boolean showImages;
  private boolean showAudios;
  private boolean showFiles;
  private boolean singleClickSelection;
  private boolean skipZeroSizeFiles;
  private int imageSize, maxSelection;
  private int landscapeSpanCount;
  private int portraitSpanCount;
  private String[] suffixes;
  private List<MediaFile> selectedMediaFiles;
  private ResultListener mListener;
  private Context mContext;
  private String title;
  private boolean showFileCount;

  public interface ResultListener {
    void onMediaFileSelected(List<MediaFile> mediaFiles);
  }

  public static FilePicker with(Context context) {
    return new FilePicker(context);
  }

  private FilePicker(Context context) {
    mContext = context;
    imageCaptureEnabled = false;
    videoCaptureEnabled = false;
    showImages = true;
    showVideos = true;
    showFiles = false;
    showAudios = false;
    singleClickSelection = true;
    skipZeroSizeFiles = true;
    imageSize = -1;
    maxSelection = -1;
    landscapeSpanCount = 5;
    portraitSpanCount = 3;

    suffixes = new String[] {
        "txt", "pdf", "html", "rtf", "csv", "xml",
        "zip", "tar", "gz", "rar", "7z", "torrent",
        "doc", "docx", "odt", "ott",
        "ppt", "pptx", "pps",
        "xls", "xlsx", "ods", "ots"
    };

    selectedMediaFiles = null;
    title = "";
    showFileCount = true;
  }

  public FilePicker resultListener(ResultListener listener) {
    this.mListener = listener;
    return this;
  }

  public FilePicker singleClickSelection(boolean singleClickSelection) {
    this.singleClickSelection = singleClickSelection;
    return this;
  }

  public FilePicker skipZeroSizeFiles(boolean skipZeroSizeFiles) {
    this.skipZeroSizeFiles = skipZeroSizeFiles;
    return this;
  }

  public FilePicker landscapeSpanCount(int landscapeSpanCount) {
    this.landscapeSpanCount = landscapeSpanCount;
    return this;
  }

  public FilePicker portraitSpanCount(int portraitSpanCount) {
    this.portraitSpanCount = portraitSpanCount;
    return this;
  }

  public FilePicker showImages(boolean showImages) {
    this.showImages = showImages;
    return this;
  }

  public FilePicker maxSelection(int maxSelection) {
    this.maxSelection = maxSelection;
    return this;
  }

  public FilePicker showVideos(boolean showVideos) {
    this.showVideos = showVideos;
    return this;
  }

  public FilePicker showFiles(boolean showFiles) {
    this.showFiles = showFiles;
    return this;
  }

  public FilePicker showAudios(boolean showAudios) {
    this.showAudios = showAudios;
    return this;
  }

  public FilePicker enableImageCapture(boolean imageCapture) {
    this.imageCaptureEnabled = imageCapture;
    return this;
  }

  public FilePicker enableVideoCapture(boolean videoCapture) {
    this.videoCaptureEnabled = videoCapture;
    return this;
  }

  public FilePicker imageSize(int imageSize) {
    this.imageSize = imageSize;
    return this;
  }

  public FilePicker suffixes(String... suffixes) {
    this.suffixes = suffixes;
    return this;
  }

  public FilePicker selectedMediaFiles(List<MediaFile> selectedMediaFiles) {
    this.selectedMediaFiles = selectedMediaFiles;
    return this;
  }

  public FilePicker selectedMediaFile(MediaFile mediaFile) {
    if(mediaFile == null) {
      this.selectedMediaFiles = null;
    } else {
      this.selectedMediaFiles = new ArrayList<>();
      this.selectedMediaFiles.add(mediaFile);
    }
    return this;
  }

  public FilePicker title(String title) {
    this.title = title;
    return this;
  }

  public FilePicker showFileCount(boolean showFileCount) {
    this.showFileCount = showFileCount;
    return this;
  }

  public void show() {
    Config configurations =
        new Config(imageCaptureEnabled, videoCaptureEnabled, showVideos, showImages,
            showAudios,
            showFiles,
            singleClickSelection, skipZeroSizeFiles, imageSize, maxSelection,
            landscapeSpanCount, portraitSpanCount, suffixes, selectedMediaFiles, title, showFileCount);

    Intent intent = new Intent(mContext, FilePickerActivity.class);
    intent.putExtra(FilePickerActivity.CONFIGS, configurations);
    mContext.startActivity(intent);
    MediaFileObservable.getInstance().addObserver(this);
  }

  @SuppressWarnings("unchecked") // suppress casting warning here.
  @Override public void update(Observable o, Object arg) {
    if (arg instanceof List<?>) {
      List<MediaFile> mediaFiles = (List<MediaFile>) arg;
      if (mListener != null) mListener.onMediaFileSelected(mediaFiles);
    }

    MediaFileObservable.getInstance().deleteObserver(this);
  }
}
