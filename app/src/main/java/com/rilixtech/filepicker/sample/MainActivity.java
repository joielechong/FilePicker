package com.rilixtech.filepicker.sample;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.rilixtech.filepicker.FilePicker;
import com.rilixtech.filepicker.MediaFile;
import java.util.ArrayList;
import java.util.List;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements FilePicker.ResultListener {
  private FileListAdapter fileListAdapter;
  private List<MediaFile> mMediaFiles = new ArrayList<>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    RecyclerView recyclerView = findViewById(R.id.file_list);
    fileListAdapter = new FileListAdapter(mMediaFiles);
    recyclerView.setAdapter(fileListAdapter);

    findViewById(R.id.launch_imagePicker).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        MainActivityPermissionsDispatcher.onImagePickerClickedWithPermissionCheck(MainActivity.this);
      }
    });

    findViewById(R.id.launch_videoPicker).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        MainActivityPermissionsDispatcher.onVideoPickerClickedWithPermissionCheck(MainActivity.this);
      }
    });

    findViewById(R.id.launch_audioPicker).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        MainActivityPermissionsDispatcher.onAudioPickerClickedWithPermissionCheck(MainActivity.this);
      }
    });

    findViewById(R.id.launch_filePicker).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        MainActivityPermissionsDispatcher.onFilePickerClickedWithPermissionCheck(MainActivity.this);      }
    });
  }


  @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
  void onImagePickerClicked() {
    FilePicker.with(MainActivity.this)
        .selectedMediaFiles(mMediaFiles)
        .enableImageCapture(true)
        .title("Hello")
        //.showFileCount(false)
        .showVideos(false)
        .skipZeroSizeFiles(true)
        .maxSelection(10)
        .resultListener(new FilePicker.ResultListener() {
          @Override public void onMediaFileSelected(List<MediaFile> mediaFiles) {
            mMediaFiles.clear();
            mMediaFiles.addAll(mediaFiles);
            fileListAdapter.notifyDataSetChanged();
          }
        })
        .show();
  }

  @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
  void onVideoPickerClicked() {
    FilePicker.with(MainActivity.this)
        .selectedMediaFiles(mMediaFiles)
        .enableVideoCapture(true)
        .showImages(false)
        .maxSelection(10)
        .resultListener(MainActivity.this)
        .show();
  }

  @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
  void onAudioPickerClicked() {
    FilePicker.with(MainActivity.this)
        .selectedMediaFiles(mMediaFiles)
        .showImages(false)
        .showVideos(false)
        .showAudios(true)
        .maxSelection(10)
        .resultListener(MainActivity.this)
        .show();
  }

  @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
  void onFilePickerClicked() {
    FilePicker.with(MainActivity.this)
        .selectedMediaFiles(mMediaFiles)
        .showFiles(true)
        .showImages(false)
        .showVideos(false)
        .maxSelection(10)
        .resultListener(MainActivity.this)
        .show();
  }

  @Override public void onMediaFileSelected(List<MediaFile> mediaFiles) {
    mMediaFiles.clear();
    mMediaFiles.addAll(mediaFiles);
    fileListAdapter.notifyDataSetChanged();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    // NOTE: delegate the permission handling to generated method
    MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
  }
}
